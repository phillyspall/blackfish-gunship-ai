/* 
	Manual - The purpose of this script is to fix the vanilla V-44X, which doesn't seem know that its guns are laterally mounted. This script should force the V-44X to behave more like an actual gunship.
	The function takes a position and spawns a Blackfish gunship to orbit around it while firing on enemies near its apex. Each parameter is explained in the parameters section.
	Features:
		-selective targeting
			The V-44X will only target enemies within its constant FOV (i.e. near the focal point of its orbit).
			The V-44X will prioritize vehicles over infantry.
		-weapon selection
			AP rounds for tanks
			Either minigun, 105mm cannon, or autocannon (HE rounds) for everything else
		-laser targeting
			When searching for a target, the V-44X will prioritize laser targets over everything else.
			The 105mm cannon is always used for laser targets. Good for clusters of infantry.
			The V-44X will NOT target the laser target until its current target has been destroyed.
			
	If you use this without significant changes, please give credit
*/

private ["_group", "_pos", "_innerRadius", "_outerRadius", "_height", "_wp", "_heli", "_crew", "_gunners", "_enemies", "_enemyVehs", "_target", "_options", "_choice", "__logic"];

openMap false;
onMapSingleClick {};

//paramaters
_pos = param [0]; //center of orbit
_innerRadius = param [1, 120]; //target zone - make sure this is small enough to remain in gunners FOV
_outerRadius = param [2, 500]; //gunship orbit radius
_height = param [3, 500]; //height ASL gunship will orbit

_group = [];
_enemies = [];
_enemyVehs = [];
_target = objNull;
_gunners = createGroup [west, false];
_options = [0, 0.4, 1, 0.2, 2, 0.4];
_logic = createGroup west createUnit ["Logic", [0,0,0], [], 0, "NONE"];

_group = [[2580.144,6928.324,(_height + 200)], 107.031, "B_T_VTOL_01_armed_F", west] call bis_fnc_spawnvehicle;
_heli = _group select 0;
_crew = _group select 1;
_crew = _group select 1;
_group = _group select 2;
[(_crew select 2)] join _gunners;
[(_crew select 3)] join _gunners;
_gunners setCombatMode "BLUE";
_gunners setBehaviour "CARELESS";
_group setBehaviour "CARELESS";


_wp = _group addWaypoint [_pos, 0];
_wp setWaypointType "LOITER";
_wp setWaypointLoiterType "CIRCLE_L";
_wp setWaypointLoiterRadius _outerRadius;
_heli flyInHeight _height;
sleep 30;
_heli flyInHeightASL [_height, _height, _height];
_heli flyInHeight 0;
_heli forceSpeed 60;

//main loop
while {alive _heli} do
{
	_enemies = allUnits select {(side _x == resistance) && (alive _x) && ((_x distance2D _pos) < _innerRadius)};
	_enemyVehs = vehicles select {((side _x == resistance) && (({alive _x} count (crew _x)) > 0) && ((_x distance2D _pos) < _innerRadius)) || ((typeOf _x == "LaserTargetW") && ((_x distance2D _pos) < _innerRadius))};
	if (((count _enemyVehs) == 0) && ((count _enemies) == 0)) then {sleep 5}
	else
	{
		if (({typeOf _x == "LaserTargetW"} count _enemyVehs) > 0) then
		{
			_logic action ["switchWeapon", _heli, (_crew select 2), 0];
			_target = (_enemyVehs select {typeOf _x == "LaserTargetW"}) select 0;
			[_heli, _target] spawn DEMO_fnc_targetLock;
			
			while {!(isNull _target)} do
			{
				_heli doTarget _target;
				sleep 5.1;
				if (!(isNull _target)) then {_logic action ["useWeapon", _heli, (_crew select 2), 0]};
			};
		}
		else
		{
			if ((count _enemyVehs) > 0) then {_target = (selectRandom _enemyVehs)}
			else {_target = (selectRandom _enemies)};
		
			[_heli, _target] spawn DEMO_fnc_targetLock;
			sleep 3;
			if (_target iskindof "Tank") then
			{
				_logic action ["switchWeapon", _heli, (_crew select 3), 5];
				while {({alive _x} count (crew _target)) > 0} do
				{
					sleep 3;
					for [{_i=0}, {_i<5}, {_i=_i+1}] do
					{
						_logic action ["useWeapon", _heli, (_crew select 3), 5];
						sleep 0.305;
					};
				};
			}
			else
			{
				while {({alive _x} count (crew _target)) > 0} do
				{
					_choice = selectRandom _options;
					if (_choice == 0) then
					{
						_logic action ["switchWeapon", _heli, (_crew select 3), 0];
						for [{_j=0}, {(_j<3) && (({alive _x} count (crew _target)) > 0)}, {_j=_j+1}] do
						{
							sleep 3;
							for [{_i=0}, {_i<5}, {_i=_i+1}] do
							{
								_logic action ["useWeapon", _heli, (_crew select 3), 0];
								sleep 0.305;
							};
						};
					};
			
					if (_choice == 1) then
					{
						_logic action ["switchWeapon", _heli, (_crew select 2), 0];
						for [{_j=0}, {(_j<2) && (({alive _x} count (crew _target)) > 0)}, {_j=_j+1}] do
						{
							sleep 5.1;
							_logic action ["useWeapon", _heli, (_crew select 2), 0];
						};
					};
			
					if (_choice == 2) then
					{
						_logic action ["switchWeapon", _heli, (_crew select 2), 5];
						for [{_j=0}, {(_j<3) && (({alive _x} count (crew _target)) > 0)}, {_j=_j+1}] do
						{
							sleep 5;
							_logic action ["useWeapon", _heli, (_crew select 2), 6];
						};
					};
				};
			};
		};
	};
};