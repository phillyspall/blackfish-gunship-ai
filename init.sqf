"spawnOne" setMarkerAlpha 0;
"base" setMarkerAlpha 0;
west setFriend [resistance, 0];
resistance setFriend [west, 0];

player setCaptive true;

player addItem "Laserdesignator";
player addItem "Laserbatteries";

myActionCAS = player addAction ["Call V-44X Blackfish",
{
	[] spawn DEMO_fnc_callCAS;
	player removeAction myActionCAS;
}];

hintSilent "Call close air support by using the scroll action and clicking on the map.";
sleep 20;
hintSilent "You can guide the 105mm cannon by using the laser designator in your inventory.";
sleep 10;
hintSilent "The gunship will target your laser after it destroys its current target";

